# encoding: UTF-8
namespace :heron do
  desc "Import versions data"

  task :country => :environment do
    ActiveRecord::Base.connection.execute "TRUNCATE spree_countries"
    ActiveRecord::Base.connection.execute "TRUNCATE spree_states"
    country = Spree::Country.create!({iso_name: 'MEXICO', name: 'México', states_required: true})

    states = Spree::State.create(
      [
        {name: 'Aguascalientes', abbr: 'AGS'},
        {name: 'Baja California', abbr: 'BCN'},
        {name: 'Baja California Sur', abbr: 'BCS'},
        {name: 'Campeche', abbr: 'CAM'},
        {name: 'Chiapas', abbr: 'CHP'},
        {name: 'Chihuahua', abbr: 'CHH'},
        {name: 'Coahuila', abbr: 'COA'},
        {name: 'Colima', abbr: 'COL'},
        {name: 'Distrito Federal', abbr: 'DIF'},
        {name: 'Durango', abbr: 'DUR'},
        {name: 'Guanajuato', abbr: 'GUA'},
        {name: 'Guerrero', abbr: 'GRO'},
        {name: 'Hidalgo', abbr: 'HID'},
        {name: 'Jalisco', abbr: 'JAL'},
        {name: 'México', abbr: 'MEX'},
        {name: 'Michoacán', abbr: 'MIC'},
        {name: 'Morelos', abbr: 'MOR'},
        {name: 'Nayarit', abbr: 'NAY'},
        {name: 'Nuevo León', abbr: 'NLE'},
        {name: 'Oaxaca', abbr: 'OAX'},
        {name: 'Puebla', abbr: 'PUE'},
        {name: 'Querétaro', abbr: 'QUE'},
        {name: 'Quintana Roo', abbr: 'ROO'},
        {name: 'San Luis Potosí', abbr: 'SLP'},
        {name: 'Sinaloa', abbr: 'SIN'},
        {name: 'Sonora', abbr: 'SON'},
        {name: 'Tabasco', abbr: 'TAB'},
        {name: 'Tamaulipas', abbr: 'TAM'},
        {name: 'Tlaxcala', abbr: 'TLA'},
        {name: 'Veracruz', abbr: 'VER'},
        {name: 'Yucatán', abbr: 'YUC'},
        {name: 'Zacatecas', abbr: 'ZAC'}
      ]
    )

    country.states = states
    country.save!
    puts "Done"
  end
end
